package com.example.corners;

import java.util.HashMap;

public class TriangleGame extends Game
{
    /**
    constructor for TriangleGame
     */
    public TriangleGame()
    {
        this.players = new HashMap<String, String>();

        this.board = new Soldier[boardSize][boardSize];

        initBoard();
    }

    /**
    this function will initialize the board
    input:
        none
    output:
        none
     */
    @Override
    public void initBoard()
    {
        int count = 0;

        for(int i = 0; i < boardSize; i++)
        {
            for(int j = 0; j < boardSize; j++)
            {
                this.board[i][j] = null;
            }
        }

        for (int i = this.boardSize - 1; i > 3; i--)
        {
            for (int j = 4 + count; j < this.boardSize; j++)
            {
                this.board[i][j] = new Soldier("b");
            }
            count ++;
        }

        count = 0;

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4 - count; j++)
            {
                this.board[i][j] = new Soldier("w");
            }
            count++;
        }
    }

    /**
    this function will check if there is a winner
    input:
        none
    output:
        boolean - if someone won
     */
    @Override
    public boolean isWin()
    {
        int count = 0;

        boolean winFlag = true;

        for (int i = this.boardSize - 1; i > 3; i--)
        {
            for (int j = 4 + count; j < 8; j++)
            {
                if(this.board[i][j] == null || this.board[i][j].getColor() != "w")
                {
                    winFlag = false;
                }
            }
            count ++;
        }

        if(winFlag) //if its not false then someone won
        {
            return true;
        }

        count = 0;

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4 - count; j++)
            {
                if(this.board[i][j] == null || this.board[i][j].getColor() != "b")
                {
                    return false;
                }
            }
            count++;
        }
        return true;
    }
}
