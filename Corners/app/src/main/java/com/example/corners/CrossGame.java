package com.example.corners;

import android.util.Log;

import java.util.HashMap;

public class CrossGame extends Game{
    /*
    constructor for CrossGame
    */
    public CrossGame()
    {
        this.players = new HashMap<String, String>();

        this.board = new Soldier[boardSize][boardSize];

        initBoard();

        String boardRepresentation = "\n";

        for(int i = 0; i < boardSize; i++)
        {
            for(int j = 0; j < boardSize; j++)
            {
                if(this.board[i][j] != null)
                {
                    boardRepresentation += this.board[i][j].getColor();
                }
                else
                {
                    boardRepresentation += "#";
                }
            }
            boardRepresentation += "\n";
        }

        Log.d("message", boardRepresentation);
    }

    /*
    this function will initialize the board
    input:
        none
    output:
        none
     */
    @Override
    public void initBoard()
    {
        for(int i = 0; i < boardSize; i++)
        {
            for(int j = 0; j < boardSize; j++)
            {
                this.board[i][j] = null;
            }
        }

        int j = 0;

        for (int i = 0; i < this.boardSize && j < this.boardSize; i++)
        {
            this.board[i][j] = new Soldier("b");
            j++;
        }

        j = this.boardSize - 1;

        for (int i = 0; i < this.boardSize && j >= 0; i++)
        {
            this.board[i][j] = new Soldier("w");
            j--;
        }
    }

    /*
    this function will check if there is a winner
    input:
        none
    output:
        boolean - if someone won
     */
    @Override
    public boolean isWin()
    {
        int j = 0;

        boolean winFlag = true;

        for (int i = 0; i < this.boardSize && j < this.boardSize; i++)
        {
            if(this.board[i][j] == null || this.board[i][j].getColor() != "w")
            {
                winFlag = false;
            }
            j++;
        }

        if(winFlag) //if its not false then someone won
        {
            return true;
        }

        j = this.boardSize - 1;

        for (int i = 0; i < this.boardSize && j >= 0; i++)
        {
            if(this.board[i][j] == null || this.board[i][j].getColor() != "b")
            {
                return false;
            }

            j--;
        }
        return true;
    }
}
