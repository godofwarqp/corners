package com.example.corners;

public class PlayerEntry
{
    private String playerName;
    private int wins;

    public PlayerEntry(){}

    public PlayerEntry( String playerName)
    {
        this.playerName = playerName;
        this.wins = 0;
    }

    public PlayerEntry(String playerName, int wins)
    {
        this.playerName = playerName;
        this.wins = wins;
    }

    //getters and setters for the playerEntry fields
    public String getPlayerName()
    {
        return this.playerName;
    }

    public void setPlayerName(String playerName)
    {
        this.playerName = playerName;
    }

    public int getWins()
    {
        return this.wins;
    }

    public void setWins(int wins)
    {
        this.wins = wins;
    }
}
