package com.example.corners;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class InstructionsActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);
    }

    /*
    this function will return to the main page
    input:
        view - the view
    output:
        none
     */
    public void backToMainPage(View view)
    {
        finish();
    }
}