package com.example.corners;

import android.util.Log;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class FireBaseManagement
{
    private final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference ref;
    private HashMap<String, PlayerEntry> players;
    private HashMap<String, PlayerEntry> topPlayers;

    public FireBaseManagement()
    {
        this.ref = this.database.getReference().child("Players");
        this.players = new HashMap<String, PlayerEntry>(); //if players is null it means that nothing has been returned from the db and its empty
        this.topPlayers = new HashMap<String, PlayerEntry>(); //if topUsers is null it means that nothing has been returned from the db and its empty
    }

    /*
    this function will retrieve the users from the database
    input:
        none
    output:
        none
     */
    public void getUsersFromDB()
    {
        database.getReference().child("Players").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                HashMap<String, PlayerEntry> map = new HashMap<>();

                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    String name = ds.child("playerName").getValue(String.class);
                    int wins = ds.child("wins").getValue(Integer.class);
                    map.put(name, new PlayerEntry(name, wins));
                }

                Log.i("sizeofmap-", Integer.toString(map.size()));

                //if the db is empty then we want to make it obvious in the code by having the user map set to null
                if(map.size() == 0)
                {
                    setPlayersMap(null);
                }
                else
                {
                    setPlayersMap(map);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e("Error", databaseError.toString());
            }
        });
    }

    /*
    this function will retrieve the top 5 users from the database with the highest scores
    input:
        none
    output:
        none
     */
    public void getTopUsersFromDB()
    {
        this.database.getReference().child("Players").orderByChild("wins").limitToLast(5).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                HashMap<String, PlayerEntry> map = new HashMap<>();

                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    String name = ds.child("playerName").getValue(String.class);
                    int wins = ds.child("wins").getValue(Integer.class);

                    map.put(name, new PlayerEntry(name, wins));
                }

                Log.i("sizeoftopmap-", Integer.toString(map.size()));

                //if the db is empty then we want to make it obvious in the code by having the user map set to null
                if(map.size() == 0)
                {
                    setTopPlayersMap(null);
                }
                else
                {
                    setTopPlayersMap(map);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e("Error", databaseError.toString());
            }
        });
    }

    /*
    this function creates a new player row
    input:
        player - the player
    output:
        none
     */
    public void addPlayer(PlayerEntry player)
    {
        if(this.players == null)
        {
            this.players = new HashMap<String, PlayerEntry>();
        }
        this.players.put(player.getPlayerName(), player);
        this.ref.setValue(this.players);
    }

    /*
    this function is called when the players map is ready
    input:
        hMap - the map with the users
    output:
        none
     */
    public void setPlayersMap(HashMap<String, PlayerEntry> hMap)
    {
        this.players = hMap;
    }

    /*
    this function is called when the top players map is ready
    input:
        hMap - the map with the users
    output:
        none
     */
    public void setTopPlayersMap(HashMap<String, PlayerEntry> hMap)
    {
        this.topPlayers = hMap;
    }

    /*
    this function returns the players map
    input:
        none
    output:
        HashMap - the map with the players
     */
    public HashMap<String, PlayerEntry> getPlayersMap()
    {
        return this.players;
    }

    /*
    this function returns the top players map
    input:
        none
    output:
        HashMap - the map with the players
     */
    public HashMap<String, PlayerEntry> getTopPlayersMap()
    {
        return this.topPlayers;
    }
}
