package com.example.corners;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class GameSettingsActivity extends AppCompatActivity {

    public static final String TYPE_GAME = "typeGame";
    public static final String FIRST_NAME = "firstName";
    public static final String SECOND_NAME = "secondName";

    public static final int REQUEST_IMAGE_CAPTURE1 = 1;
    public static final int REQUEST_IMAGE_CAPTURE2 = 2;

    private RadioButton rbTriangle;
    private RadioButton rbRectangle;
    private RadioButton rbCross;
    private EditText etFirstName;
    private EditText etSecondName;
    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor editor;
    private ActivityResultLauncher<Void> mGetThumb1;
    private ActivityResultLauncher<Void> mGetThumb2;
    private ImageView ivPlayer1;
    private ImageView ivPlayer2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_settings);

        this.rbRectangle = (RadioButton)findViewById(R.id.rectangleOption);
        this.rbTriangle = (RadioButton)findViewById(R.id.triangleOption);
        this.rbCross = (RadioButton)findViewById(R.id.crossOption);

        this.etFirstName = (EditText)findViewById(R.id.firstName);
        this.etSecondName = (EditText)findViewById(R.id.secondName);

        this.sharedPrefs = this.getSharedPreferences(String.valueOf(R.string.shared_prefs_file_name), MODE_PRIVATE);
        this.editor = sharedPrefs.edit();

        this.etFirstName.setText(sharedPrefs.getString(String.valueOf(R.string.shared_prefs_player_1_name), ""));
        this.etSecondName.setText(sharedPrefs.getString(String.valueOf(R.string.shared_prefs_player_2_name), ""));

        this.ivPlayer1 = (ImageView)findViewById(R.id.ivPlayer1);
        this.ivPlayer2 = (ImageView)findViewById(R.id.ivPlayer2);

        this.mGetThumb1 = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                ImageView imageView = findViewById(R.id.ivPlayer1);
                imageView.setImageBitmap(result);
            }});

        this.mGetThumb2 = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                ImageView imageView = findViewById(R.id.ivPlayer2);
                imageView.setImageBitmap(result);
            }});

    }

    public void onClick(View view)
    {
        int typeGame = 0;

        if(rbTriangle.isChecked())
        {
            typeGame = 0; //0 for triangle style game, 1 for rectangle, 2 for cross
        }
        else if(rbRectangle.isChecked())
        {
            typeGame = 1;
        }
        else if(rbCross.isChecked())
        {
            typeGame = 2;
        }

        String firstName = this.etFirstName.getText().toString();
        String secondName = this.etSecondName.getText().toString();
        Intent gameIntent = new Intent(this, GameActivity.class);

        if(firstName.trim().equals("") || secondName.trim().equals(""))
        {
            Toast.makeText(this, "Player names must be filled!", Toast.LENGTH_SHORT).show();
        }
        else if(firstName.trim().equals(secondName.trim()))
        {
            Toast.makeText(this, "Player names must not be the same!", Toast.LENGTH_SHORT).show();
        }
        else //if everything is valid, can put pictures in files and save them for later use in next activity.
        {
            this.editor.putString(String.valueOf(R.string.shared_prefs_player_1_name), firstName);
            this.editor.putString(String.valueOf(R.string.shared_prefs_player_2_name), secondName);
            editor.apply();

            //read images
            Bitmap image1 = ((BitmapDrawable) ivPlayer1.getDrawable()).getBitmap();
            Bitmap image2 = ((BitmapDrawable) ivPlayer2.getDrawable()).getBitmap();

            //open file
            FileOutputStream fileOutputStream1, fileOutputStream2;
            //write image to file

            try
            {
                fileOutputStream1 = openFileOutput(getString(R.string.profile_pic1), MODE_PRIVATE);
                fileOutputStream2 = openFileOutput(getString(R.string.profile_pic2), MODE_PRIVATE);

                image1.compress(Bitmap.CompressFormat.JPEG, 50, fileOutputStream1);
                image2.compress(Bitmap.CompressFormat.JPEG, 50, fileOutputStream2);

                fileOutputStream1.flush();
                fileOutputStream2.flush();

                //close file
                fileOutputStream1.close();
                fileOutputStream2.close();
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            gameIntent.putExtra(TYPE_GAME, typeGame);
            gameIntent.putExtra(FIRST_NAME, firstName);
            gameIntent.putExtra(SECOND_NAME, secondName);

            startActivity(gameIntent);
            finish();
        }
    }
    /*
    this function will take the first picture
    input:
        view - the view
    output:
        none
     */
    public void takePic1(View view)
    {
        this.mGetThumb1.launch(null);
    }

    /*
    this function will take the first picture
    input:
        view - the view
    output:
        none
     */
    public void takePic2(View view)
    {
        this.mGetThumb2.launch(null);
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        //check if there's an image
        if(requestCode == REQUEST_IMAGE_CAPTURE1)
        {
            if (resultCode == RESULT_OK)
            {
                Bitmap imageBitmap = (Bitmap)data.getExtras().get("data");
                this.ivPlayer1.setImageBitmap(imageBitmap);
            }
        }
        else if(requestCode == REQUEST_IMAGE_CAPTURE2)
        {
            if (resultCode == RESULT_OK)
            {
                Bitmap imageBitmap = (Bitmap)data.getExtras().get("data");
                this.ivPlayer2.setImageBitmap(imageBitmap);
            }
        }
    }
}