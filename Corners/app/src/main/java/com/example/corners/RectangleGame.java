package com.example.corners;

import java.util.HashMap;

public class RectangleGame extends Game
{
    /*
    constructor for RectangleGame
     */
    RectangleGame()
    {
        this.players = new HashMap<String, String>();

        this.board = new Soldier[boardSize][boardSize];

        initBoard();
    }

    /*
    this function will initialize the board
    input:
        none
    output:
        none
     */
    @Override
    public void initBoard()
    {
        for(int i = 0; i < boardSize; i++)
        {
            for(int j = 0; j < boardSize; j++)
            {
                this.board[i][j] = null;
            }
        }

        for (int i = this.boardSize - 1; i >= this.boardSize / 2 + 1; i--)
        {
            for (int j = this.boardSize - 1; j >= this.boardSize / 2; j--)
            {
                this.board[i][j] = new Soldier("b");
            }
        }


        for (int i = 0; i < this.boardSize / 2 - 1; i++)
        {
            for (int j = 0; j < this.boardSize / 2; j++)
            {
                this.board[i][j] = new Soldier("w");
            }
        }
    }

    /*
    this function will check if there is a winner
    input:
        none
    output:
        boolean - if someone won
     */
    @Override
    public boolean isWin()
    {
        boolean winFlag = true;

        for (int i = this.boardSize - 1; i > this.boardSize / 2; i--)
        {
            for (int j = this.boardSize - 1; j >= this.boardSize / 2; j--)
            {
                if(this.board[i][j] == null || this.board[i][j].getColor() != "w")
                {
                    winFlag = false;
                }
            }
        }

        if(winFlag) //if its not false then someone won
        {
            return true;
        }

        for (int i = 0; i < this.boardSize / 2 - 2; i++)
        {
            for (int j = 0; j < this.boardSize / 2 - 1; j++)
            {
                if(this.board[i][j] == null || this.board[i][j].getColor() != "b")
                {
                    return false;
                }
            }
        }
        return true;
    }
}
