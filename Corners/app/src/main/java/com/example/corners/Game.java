package com.example.corners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

abstract public class Game
{
    protected static final int boardSize = 8;
    protected Soldier[][] board;
    protected String turn;
    protected HashMap<String, String> players;

    /*
    set the order of players, who will be white and who will be black
    input:
        firstName - the name of the first player
        secondName - the name of the second player
    output:
        none
     */
    public void play(String firstName, String secondName)
    {

        int rnd = new Random().nextInt(2) + 1;

        if (rnd == 1) {
            players.put("w", firstName);
            players.put("b", secondName);
        }
        else
        {
            players.put("b", firstName);
            players.put("w", secondName);
        }

        this.turn = "w";
    }

    /*
    this function will move the soldier on the board
    input:
        source - Object, represents the source location
        destination - Object, represents the destination location
    output:
        if the soldier moved or not
     */
    public boolean move(Object source, Object destination)
    {
        boolean result = false;

        int sourceI =Integer.parseInt(((String)source).split(",")[0]);
        int sourceJ =Integer.parseInt(((String)source).split(",")[1]);

        int destI =Integer.parseInt(((String)destination).split(",")[0]);
        int destJ =Integer.parseInt(((String)destination).split(",")[1]);

        if((sourceI + 1 == destI && sourceJ == destJ) //if the destination is within one square from the source
                || (sourceI == destI && sourceJ - 1 == destJ)
                || (sourceI - 1 == destI && sourceJ == destJ)
                || (sourceI == destI && sourceJ + 1 == destJ))
        {
            result = canMove(source, destination, true); //isNear = true
        }
        else
        {
            result = canMove(source, destination, false); //isNear = false
        }

        if(result)
        {
            board[destI][destJ] = board[sourceI][sourceJ];
            board[sourceI][sourceJ] = null;

            if(this.turn == "w")
            {
                this.turn = "b";
            }
            else
            {
                this.turn = "w";
            }

            return true;//move the soldier on the board
        }
        else
        {
            return false;//the soldier should not move
        }
    }

    /*
    this function checks if the soldier can move to a location
    input:
        source - Object, represents the source location
        destination - Object, represents the destination location
        isNear - boolean, whether the move location is within one square of the source location
    output:
        if the soldier can move or not
     */
    public boolean canMove(Object source, Object destination, boolean isNear)
    {
        int sourceI = Integer.parseInt(((String)source).split(",")[0]);
        int sourceJ = Integer.parseInt(((String)source).split(",")[1]);

        int destI = Integer.parseInt(((String)destination).split(",")[0]);
        int destJ = Integer.parseInt(((String)destination).split(",")[1]);

        if(board[sourceI][sourceJ] == null) //if the player chose a blank spot
        {
            return false;
        }

        if(board[sourceI][sourceJ] != null && board[sourceI][sourceJ].getColor() != this.turn) //if it is not the players soldier
        {
            return false;
        }

        if(board[destI][destJ] != null) //if there is already a soldier on that spot
        {
            return false;
        }

        if(sourceI == destI && sourceJ == destJ) //if player is trying to move a soldier to itself
        {
            return false;
        }

        if(isNear) //if the location is near the soldier +- 1 square
        {
            if(board[destI][destJ] == null)
            {
                return true;
            }
        }
        else
        {
            return recursiveCanMove(new ArrayList<Object>(), sourceI, sourceJ, destI, destJ, sourceI, sourceJ);
        }

        return false;
    }

    /*
    this function will recursively check if the soldier can move to the selected location by recursively going through all the options of hopping over other soldiers
    input:
        sourceI - this variable represents the row number of the previous square the soldier was on
        sourceJ - this variable represents the column number of the previous square the soldier was on
        destI - this variable represents the destination row number
        destJ - this variable represents the destination column number
        i - this variable represents the current row number
        j - this variable represents the current column number
    output:
        boolean - if the move location is valid or not
     */
    public boolean recursiveCanMove(ArrayList<Object> placesBeen, int sourceI, int sourceJ, int destI, int destJ, int i, int j) // i, j are current coordinates
    {
        if(i == destI && j == destJ)
        {
            return true;
        }

        if(placesBeen.contains(i + "," + j)) //checking if the current i, j were already used to go somewhere
        {
            return false;
        }

        placesBeen.add(i + "," + j);

        if(i + 1 < this.boardSize && this.board[i + 1][j] != null)
        {
            if((i + 2 != sourceI) && (i + 2 < this.boardSize && this.board[i + 2][j] == null)) //(i + 2 != sourceI): checking if the previous location is not considered as a next jumping spot
            {
                if(recursiveCanMove(placesBeen, i, j, destI, destJ, i + 2, j))
                {
                    return true;
                }
            }
        }
        if(j - 1 >= 0 && this.board[i][j - 1] != null)
        {
            if((j - 2 != sourceJ) && j - 2 >= 0 && this.board[i][j - 2] == null)
            {
                if(recursiveCanMove(placesBeen, i, j, destI, destJ, i, j - 2))
                {
                    return true;
                }
            }
        }
        if(i - 1 >= 0 && this.board[i - 1][j] != null)
        {
            if((i - 2 != sourceI) && i - 2 >= 0 && this.board[i - 2][j] == null)
            {
                if(recursiveCanMove(placesBeen, i, j, destI, destJ, i - 2, j))
                {
                    return true;
                }
            }
        }
        if(j + 1 < this.boardSize && this.board[i][j + 1] != null)
        {
            if((j + 2 != sourceJ) && j + 2 < this.boardSize && this.board[i][j + 2] == null)
            {
                if(recursiveCanMove(placesBeen, i, j, destI, destJ, i, j + 2))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public abstract boolean isWin();
    public abstract void initBoard();

    public HashMap<String, String> getPlayers()
    {
        return this.players;
    }

    public Soldier[][] getBoard()
    {
        return this.board;
    }

    public String getTurn()
    {
        return this.turn;
    }
}
