package com.example.corners;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class BgMusic extends Service //this class is responsible for the background music in the game.
{
    private static final String TAG = null;
    MediaPlayer player; //the media player

    public IBinder onBind(Intent arg0) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.lofibgmusic);
        player.setLooping(true); // Set looping
        player.setVolume(100, 100);

    }

    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if(this.player.isPlaying())
        {
            player.pause();
        }
        else
        {
            player.start();
        }
        return Service.START_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        player.start();
    }

    public IBinder onUnBind(Intent arg0) {
        return null;
    }

    public void onStop()
    {
        player.stop();
    }

    public void onPause() {
        if(player != null && player.isPlaying())
        {
            player.pause();
        }
    }

    @Override
    public void onDestroy()
    {
        player.stop();
        player.release();
    }

    @Override
    public void onLowMemory()
    {

    }
}