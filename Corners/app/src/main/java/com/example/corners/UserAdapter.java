package com.example.corners;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;


public class UserAdapter extends ArrayAdapter<PlayerEntry>
{
    private ArrayList<PlayerEntry> list;
    private Context context;

    public UserAdapter(@NonNull Context context, ArrayList<PlayerEntry> list)
    {
        super(context, R.layout.leader_board_row, list);

        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int i, @Nullable View view, @NonNull ViewGroup viewGroup) {
        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();

        if(view == null)
        {
            view = inflater.inflate(R.layout.leader_board_row, viewGroup, false);
        }

        TextView tvName = view.findViewById(R.id.tvCustomListName);
        TextView tvScore = view.findViewById(R.id.tvCustomListScore);

        PlayerEntry p = list.get((i));

        tvName.setText(p.getPlayerName());
        tvScore.setText(String.valueOf(p.getWins()));

        return view;
    }
}
