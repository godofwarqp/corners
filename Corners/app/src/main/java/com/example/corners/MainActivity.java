package com.example.corners;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private Button instructionsButton;
    private Button leaderBoardButton;
    private Button playButton;
    private ToggleButton MusicButton;
    private static final int READ_PHONE_STATE_PERMISSION = 1;
    public static boolean isPlaying = false; //this variable tells if the music is playing or not

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MusicButton = (ToggleButton) findViewById(R.id.toggleButton);

        startMusic();

        this.leaderBoardButton = findViewById(R.id.leaderBoardButton);
        this.instructionsButton = findViewById(R.id.instructionsButton);
        this.playButton = findViewById(R.id.playButton);

        this.leaderBoardButton.setOnClickListener(this);
        this.instructionsButton.setOnClickListener(this);
        this.playButton.setOnClickListener(this);
        phonePermission();

    }

    /*
    this function will check if permission to access phone calls is already granted
    input:
        none
    output:
        none
     */
    public void phonePermission()
    {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
        {
            Log.d("phonePermission", "granted");
        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_PERMISSION);
        }
    }

    /*
    this function will ask the user for permission to manage calls
    input:
        requestCode - int
        permissions - string array
        grantedResults - integer array

    output:
        none
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode)
        {
            case READ_PHONE_STATE_PERMISSION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /*
    this function starts the music if the button is checked
    input:
        none
    output:
        none
     */
    private void startMusic()
    {
        MusicButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (MusicButton.isChecked()) {
                    isPlaying = true; //this means that the music started
                    Intent myService = new Intent(MainActivity.this, BgMusic.class);
                    startService(myService);
                } else {
                    isPlaying = false; //this means that there is no music
                    Intent myService = new Intent(MainActivity.this, BgMusic.class);
                    stopService(myService);
                }
            }
        });
    }

    public void onClick(View view)
    {
        int id = view.getId();

        switch(id)
        {
            case R.id.instructionsButton:
                Intent instructionsIntent = new Intent(this, InstructionsActivity.class);
                startActivity(instructionsIntent);
                break;
            case R.id.leaderBoardButton:
                Intent leaderBoardIntent = new Intent(this, LeaderBoardActivity.class);
                startActivity(leaderBoardIntent);
                break;
            case R.id.playButton:
                Intent playIntent = new Intent(this, GameSettingsActivity.class);
                startActivity(playIntent);
                break;
        }
    }
}