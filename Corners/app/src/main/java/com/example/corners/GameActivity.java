package com.example.corners;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;

public class GameActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int boardSize = 8;
    private LinearLayout llBoard;
    private int typeGame;
    private ImageView previousSquare; //this attribute is a pointer to the square clicked on prior to the current square, in order to remove the purple paint.
    private int previousColor; //this is the color of the previous square.
    private Game game;
    private HashMap<String, String> players;
    private String firstName;
    private String secondName;
    private TextView tvTurnText;
    private TextView tvNameToColor;
    private ImageView ivPlayer;
    private FireBaseManagement fm; //the class which is responsible for managing the database
    HashMap<String, PlayerEntry> users;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //removing the title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //making the game fullscreen

        setContentView(R.layout.activity_game);

        this.fm = new FireBaseManagement();
        this.fm.getUsersFromDB();

        Intent intent = getIntent();

        this.llBoard = (LinearLayout)findViewById(R.id.llBoard);
        this.tvTurnText = (TextView)findViewById(R.id.turnText);
        this.tvNameToColor = (TextView)findViewById(R.id.nameToColor);

        this.typeGame = intent.getIntExtra(GameSettingsActivity.TYPE_GAME, 0);
        this.firstName = intent.getStringExtra(GameSettingsActivity.FIRST_NAME);
        this.secondName = intent.getStringExtra(GameSettingsActivity.SECOND_NAME);

        this.players = new HashMap<String, String>();

        this.ivPlayer = (ImageView)findViewById(R.id.ivPlayer);
        startGame();
    }

    /*
   this function will open the menu
   input:
       menu - the menu
   output:
       boolean - true
    */
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);

        return true;
    }

    /*
    this function determines what to do when options are selected
    input:
        item - the item selected
    output:
        boolean - true
     */
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        menuEnd();

        return true;
    }

    /*
    this function pops a screen asking if the user wants to leave to the main page
    input:
        none
    output:
        none
     */
    private void menuEnd()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Exit game?");
        alertDialog.setMessage("Are you sure you want to exit this game?");
        alertDialog.setCancelable(true);

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                dialogInterface.dismiss();
            }
        });

        alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    /*
    this function sets the image of the current player
    input:
        turn - whose turn is it
    output:
        none
     */
    private void setImagePic(String turn)
    {
        File file;
        int id;

        if(this.firstName.equals(this.players.get(turn)))
        {
            id = R.string.profile_pic1;
        }
        else
        {
            id = R.string.profile_pic2;
        }
        file = new File(getFilesDir(), getString(id));

        if(file.exists())
        {
            FileInputStream fis = null;

            try
            {
                fis = openFileInput(getString(id));
                Bitmap bitmap = BitmapFactory.decodeStream(fis);
                this.ivPlayer.setImageBitmap(bitmap);
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
        }
    }

    /*
     this function starts the game
     input:
        none
     output:
        none
     */
    private void startGame()
    {
        switch(this.typeGame)
        {
            case 0:
                this.game = new TriangleGame();
                break;
            case 1:
                this.game = new RectangleGame();
                break;
            case 2:
                this.game = new CrossGame();
                break;
        }

        this.game.play(this.firstName, this.secondName);

        this.players = this.game.getPlayers();

        this.tvNameToColor.setText("White: " + this.players.get("w") + "\nBlack: " + this.players.get("b"));

        this.tvTurnText.setText("Turn: " + players.get("w"));
        setImagePic(this.game.getTurn());
        displayBoard();
    }

    /*
    this function will draw the board
    input:
        none
    output:
        none
     */
    private void displayBoard()
    {
        int squareColor = R.color.beige;

        LinearLayout linearLayoutBoard = new LinearLayout(this);
        linearLayoutBoard.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayoutBoard.setOrientation(LinearLayout.VERTICAL);

        DisplayMetrics metrics = getResources().getDisplayMetrics();

        int screenWidth = metrics.widthPixels;

        int imageViewWidth = screenWidth / 8; //in order to fit the whole screen
        int imageViewHeight = imageViewWidth; //in order to fit the whole screen
        int rowMargin = 0;

        int imageViewMargin = 0;

        LinearLayout rowInBoard;

        LinearLayout.LayoutParams rowLayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rowLayout.setMargins(rowMargin, 0, rowMargin, 0);

        LinearLayout.LayoutParams imageViewLayout = new LinearLayout.LayoutParams(imageViewWidth, imageViewHeight);
        imageViewLayout.setMargins(imageViewMargin, 0, imageViewMargin, 0);

        for (int i = 0; i < boardSize; i++)
        {
            rowInBoard = new LinearLayout(this);
            rowInBoard.setLayoutParams(rowLayout);
            rowInBoard.setOrientation(LinearLayout.HORIZONTAL);

            for (int j = 0; j < boardSize; j++)
            {
                if(i % 2 == 0 && j % 2 == 0)
                {
                    squareColor = R.color.blue;


                }
                else if(i % 2 != 0 && j % 2 != 0)
                {
                    squareColor = R.color.blue;
                }
                else
                {
                    squareColor = R.color.beige;
                }

                ImageView iv = new ImageView(this);
                iv.setLayoutParams(imageViewLayout);
                iv.setBackgroundResource(squareColor);
                iv.setTag(i + "," + j);
                iv.setOnClickListener(this);
                rowInBoard.addView(iv);
            }
            linearLayoutBoard.addView(rowInBoard);
        }
        this.llBoard.addView(linearLayoutBoard);

        placePawnsOnBoard();
    }

    /*
     this function will place the pawns on the board
     input:
        none
     output:
        none
     */
    private void placePawnsOnBoard()
    {
        Soldier[][] board = this.game.getBoard();
        LinearLayout layout = (LinearLayout) llBoard.getChildAt(0);
        LinearLayout layout2 = null;

        for (int i = 0; i < this.boardSize; i++)
        {

            layout2 = (LinearLayout) layout.getChildAt(i);
            for (int j = 0; j < this.boardSize; j++)
            {
                if(board[i][j] != null && board[i][j].getColor() == "b")
                {
                    ((ImageView) layout2.getChildAt(j)).setImageResource(R.drawable.black_pawn);
                }
                else if(board[i][j] != null && board[i][j].getColor() == "w")
                {
                    ((ImageView) layout2.getChildAt(j)).setImageResource(R.drawable.white_pawn);
                }
            }
        }
    }

    /*
    this function is the onclick function for the clicks on the board
    input:
        view - the square on the board
    output:
        none
     */
    @Override
    public void onClick(View view)
    {
        changeSelectedSquareColor(view);

        if(((ColorDrawable)view.getBackground()).getColor() != getResources().getColor(R.color.purple_200))
        {
            if(this.game.move(this.previousSquare.getTag(), view.getTag()))
            {
                if(this.game.getTurn() == "w") //the turn has changed if the move happened. check what the turn is now, to know what color was moved before.
                {
                    ((ImageView) view).setImageResource(R.drawable.black_pawn);
                }
                else if(this.game.getTurn() == "b")
                {
                    ((ImageView) view).setImageResource(R.drawable.white_pawn);
                }

                setImagePic(this.game.getTurn()); //putting the image of the player whose turn it is now

                if(this.game.isWin())
                {
                    freezeBoard();
                    PlayerEntry p;

                    //this loop is in order to wait for the users list to be returned from the database before changing stats
                    do
                    {
                        this.users = this.fm.getPlayersMap();

                    }while(this.users != null && this.users.size() == 0);

                    if(this.game.getTurn() == "w") //because the turn has changed once the soldier moved, the person who won is the opposite of the current turn.
                    {
                        Toast.makeText(this, "black won!", Toast.LENGTH_LONG).show();
                        this.tvTurnText.setText("Winner: " + this.players.get("b"));
                        setImagePic("b"); //because the turn has changed we get the other player

                        p = new PlayerEntry(this.players.get("b"));
                    }
                    else
                    {
                        Toast.makeText(this, "white won!", Toast.LENGTH_LONG).show();
                        this.tvTurnText.setText("Winner: " + this.players.get("w"));
                        setImagePic("w"); //because the turn has changed we get the other player

                        p = new PlayerEntry(this.players.get("w"));
                    }

                    //updating the player win number in the database
                    if(this.users != null && this.users.containsKey(p.getPlayerName()))
                    {
                        Log.i("player in db name - ", p.getPlayerName());
                        p.setWins((this.users.get(p.getPlayerName())).getWins() + 1);
                    }
                    else
                    {
                        p.setWins(1);
                    }

                    this.fm.addPlayer(p);
                }
                else
                {
                    this.tvTurnText.setText("Turn: " + this.players.get(this.game.getTurn()));
                }

                this.previousSquare.setImageDrawable(null);
            }

           this.previousSquare = null;
        }
    }

    /*
    this function will change the color of the selected square
    input:
        view - the square on the board
    output:
        none
     */
    private void changeSelectedSquareColor(View view)
    {
        ImageView img = (ImageView)view;

        boolean isPrevPurple = false;

        if(this.previousSquare == null)
        {
            this.previousSquare = img;
            this.previousColor = ((ColorDrawable)this.previousSquare.getBackground()).getColor();
        }

        if( ((ColorDrawable)this.previousSquare.getBackground()).getColor() == getResources().getColor(R.color.purple_200))
        {
            isPrevPurple = true;
            this.previousSquare.setBackgroundColor(this.previousColor);
        }

        if(!isPrevPurple)
        {
            img.setBackgroundResource(R.color.purple_200);
        }
    }

    /*
    this function will freeze the board so that the players can no longer move their soldiers
    input:
        none
    output:
        none
     */
    private void freezeBoard() //this function will make it so that the user can not move any soldiers if the game has ended
    {
        LinearLayout layout = (LinearLayout) llBoard.getChildAt(0);
        LinearLayout layout2 = null;

        for (int i = 0; i < boardSize; i++)
        {
            layout2 = (LinearLayout) layout.getChildAt(i);
            for (int j = 0; j < boardSize; j++)
            {
                layout2.getChildAt(j).setOnClickListener(null);
            }
        }
    }
}