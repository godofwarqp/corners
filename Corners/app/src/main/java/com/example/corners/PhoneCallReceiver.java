package com.example.corners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import static com.example.corners.MainActivity.isPlaying;

public class PhoneCallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.d("PhoneCallReceiver", "received call");

        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                Toast.makeText(context, "Incoming call!! Go ahead, the game will wait!", Toast.LENGTH_LONG).show();
                stopServices(context);
            }

            if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                Toast.makeText(context, "Received State", Toast.LENGTH_SHORT).show();
            }

            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                Toast.makeText(context, "Idle state: Ringing stopped.", Toast.LENGTH_SHORT).show();
                startServices(context);
            }
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }

    }

    /*
    this function stops the background music when there is an incoming call
    input:
        con - the context
    output:
        none
     */
    private void stopServices(Context con)
    {
        if(isPlaying)
        {
            Intent i = new Intent(con, BgMusic.class);
            con.startService(i);
        }
    }

    /*
    this function starts the background music when the call is finished
    input:
        con - the context
    output:
        none
     */
    private void startServices(Context con)
    {
        if (isPlaying) //checks if the music was turned on
        {
            Intent i = new Intent(con, BgMusic.class);
            con.startService(i);
        }
    }
}