package com.example.corners;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.HashMap;

public class LeaderBoardActivity extends AppCompatActivity {

    private FireBaseManagement fb;
    private HashMap<String, PlayerEntry> players;
    private ListView lvLeaderBoard;
    private UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);

        this.fb = new FireBaseManagement();
        this.fb.getTopUsersFromDB();

        this.players = new HashMap<String, PlayerEntry>();

        this.lvLeaderBoard = findViewById(R.id.lvLeaderBoard);

        showListOfUsers();
    }

    /*
    this function shows the list of users on the LeaderBoard activity
    input:
        none
    output:
        none
     */
    public void showListOfUsers()
    {
        ArrayList<PlayerEntry> playerList = new ArrayList<>();

        Log.i("sizeof", "here");

        this.players = this.fb.getTopPlayersMap();

        if(this.players != null && this.players.size() != 0)
        {
            PlayerEntry[] playerArr = new PlayerEntry[this.players.size()];

            int i = 0;

            playerList.addAll(this.players.values());

            for(i = 0; i < playerList.size(); i++)
            {
                playerArr[i] = playerList.get(i);
            }

            playerList.clear();

            //sorting the users by their scores
            playerArr = bubbleSortList(playerArr);

            //putting the sorted users back in the list and sending it to the adapter
            for(i = 0; i < playerArr.length; i++)
            {
                playerList.add(playerArr[i]);
            }

            this.adapter = new UserAdapter(this, playerList);
            this.lvLeaderBoard.setAdapter(this.adapter);
        }
    }

    /*
    this function bubble sorts the array of users
    input:
        playerArr - PlayerEntry, the array to sort
    output:
        the sorted array
     */
    private PlayerEntry[] bubbleSortList(PlayerEntry[] playerArr)
    {
        PlayerEntry p;

        for(int i = 0; i < playerArr.length; i++)
        {
            for(int j = 1; j < playerArr.length; j++)
            {
                if(playerArr[j - 1].getWins() < playerArr[j].getWins())
                {
                    p = playerArr[j];
                    playerArr[j] = playerArr[j - 1];
                    playerArr[j - 1] = p;
                }
            }
        }

        return playerArr;
    }

    /*
    this function will refresh the leader board when a button is pressed
    input:
        view - the button
    output:
        none
     */
    public void refresh(View view)
    {
        showListOfUsers();
    }

    /*
    this function will return the user to the main page when the button is pressed
    input:
        none
    output:
        none
     */
    public void backToMainPage(View view)
    {
        finish();
    }
}