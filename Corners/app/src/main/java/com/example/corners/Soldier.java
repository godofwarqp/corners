package com.example.corners;

public class Soldier
{
    private String color;

    public Soldier()
    {
        this.color = "w";//default color of a soldier
    }
    public Soldier(String color)
    {
        this.color = color;
    }

    /*
    this fucntion returns the color of the soldier
    input:
        none
    output:
        string, the color
     */
    public String getColor()
    {
        return this.color;
    }
}
